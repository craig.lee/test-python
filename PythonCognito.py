import requests

def authenticate_with_cognito(username, password, client_id, client_secret, user_pool_id, app_client_id, aws_region):
    # AWS Cognito endpoint
    cognito_url = f'https://cognito-idp.{aws_region}.amazonaws.com/{user_pool_id}'
    
    # OAuth 2.0 token endpoint
    token_url = f'{cognito_url}/oauth2/token'

    # Request payload
    data = {
        'grant_type': 'password',
        'client_id': client_id,
        'client_secret': client_secret,
        'username': username,
        'password': password,
    }

    # Headers
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }

    # Send POST request to authenticate
    response = requests.post(token_url, data=data, headers=headers)

    # Check if request was successful
    if response.status_code == 200:
        # Parse and print access token
        access_token = response.json().get('access_token')
        print(f'Access Token: {access_token}')
    else:
        # Print error message
        print(f'Error: {response.status_code} - {response.text}')

# Replace placeholders with your actual credentials and details
username = 'YOUR_USERNAME'
password = 'YOUR_PASSWORD'
client_id = 'YOUR_CLIENT_ID'
client_secret = 'YOUR_CLIENT_SECRET'
user_pool_id = 'YOUR_USER_POOL_ID'
app_client_id = 'YOUR_APP_CLIENT_ID'
aws_region = 'YOUR_AWS_REGION'

# Authenticate with AWS Cognito
authenticate_with_cognito(username, password, client_id, client_secret, user_pool_id, app_client_id, aws_region)
